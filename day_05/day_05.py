import math

def get_codes():
    codes = []
    with open('./day_05/input.txt') as f:
        codes = f.readlines()
    return codes


def decode_row(code):
    # print(code)
    lower_bound = 0
    upper_bound = 127
    for bit in code:
        if bit == 'F':
            upper_bound = upper_bound - math.ceil((upper_bound - lower_bound) / 2)
        elif bit == 'B':
            lower_bound = lower_bound + math.ceil((upper_bound - lower_bound) / 2)

    return upper_bound

def decode_col(code):
    # print(code)
    lower_bound = 0
    upper_bound = 7

    for bit in code:
        if bit == 'L':
            upper_bound = upper_bound - math.ceil((upper_bound - lower_bound) / 2)
        elif bit == 'R':
            lower_bound = lower_bound + math.ceil((upper_bound - lower_bound) / 2)

    return upper_bound

def decode(code):
    row = decode_row(code[:7])
    col = decode_col(code[7:])

    return row, col

if __name__ == "__main__":
    highest_id = 0

    codes = get_codes()
    for code in codes:
        row, col = decode(code.strip())
        id = row * 8 + col
        if id > highest_id:
            highest_id = id

    print(f"Part 1: {highest_id}")

    ids = []    
    for code in codes:
        row, col = decode(code.strip())
        ids.append(row * 8 + col)

    ids.sort()

    part2_id = None
    for id1, id2 in list(zip(ids, ids[1:])):
        if id2 - id1 != 1:
            print(f'ID1: {id1} - ID2: {id2}')
            part2_id = id1 + 1

    print(f'Part 2: {part2_id}')