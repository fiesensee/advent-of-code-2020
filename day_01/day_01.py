def get_numbers():
    numbers = []
    with open('./day_01/input.txt') as f:
        numbers = f.readlines()
    return numbers

def find_numbers(lines):
    for line1 in lines:
        for line2 in lines:
            for line3 in lines:
                if int(line1) + int(line2) + int(line3) == 2020:
                    print(line1)
                    print(line2)
                    print(line3)
                    print(int(line1) * int(line2) * int(line3))

if __name__ == "__main__":
    numbers = get_numbers()
    find_numbers(numbers)