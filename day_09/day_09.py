def get_input():
    input = []
    with open('./day_09/input.txt') as f:
        input = [int(line.strip('\n')) for line in f]
    return input

def is_sum(numbers, target):
    snumbers = list(filter(lambda x: x < target, numbers))
    # print(target)
    # print(snumbers)
    for number1 in snumbers:
        for number2 in snumbers:
            # print(number1, number2, number1 + number2)
            if number1 == number2:
                continue
            if number1 + number2 == target:
                return True
    return False


if __name__ == "__main__":
    input = get_input()
    preamble = 25

    invalid_number = None
    invalid_number_position = None

    for x in range(len(input)):
        current_index = preamble + x
        number = input[current_index]
        if not is_sum(input[current_index - preamble:current_index], number):
            print(f'Part 1: {number}')
            invalid_number = number
            invalid_number_position = current_index
            break
    
    print(invalid_number_position)

    part2_numbers = input[:invalid_number_position]
    part2_numbers.reverse()

    part2_range = []

    start_index = 0

    while part2_range == []:
        sum = part2_numbers[start_index]
        end_index = start_index + 1
        while sum <= invalid_number:
            sum += part2_numbers[end_index]
            if sum == invalid_number:
                print(part2_numbers[start_index], part2_numbers[end_index])
                part2_range = part2_numbers[start_index:end_index + 1]
                break
            end_index += 1
        start_index += 1

    part2 = max(part2_range) + min(part2_range)

    print(f'Part 2: {part2}')