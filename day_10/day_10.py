import functools

def get_input():
    input = []
    with open('./day_10/input.txt') as f:
        input = [int(line.strip('\n')) for line in f]
    return input

def get_next_jolt_by_diff(jolts, start, diff):
    for jolt in jolts:
        if jolt == start + diff:
            return jolt

@functools.lru_cache(maxsize=None)
def get_next_jolts_by_diff_reverse(current_jolt):
    next_jolts = []
    for jolt in input:
        if jolt >= current_jolt - 3 and jolt < current_jolt:
            next_jolts.append(jolt)

    return next_jolts

@functools.lru_cache(maxsize=None)
def get_possible_combinations(current_jolt):
    next_jolts = get_next_jolts_by_diff_reverse(current_jolt)
    sub_total = 0
    if current_jolt <= 3:
        sub_total = 1
    for jolt in next_jolts:
        sub_total += get_possible_combinations(jolt)

    return sub_total

if __name__ == "__main__":
    global input
    input = get_input()
    input.sort()
    print(input)

    jolt_diffs_1 = 0
    jolt_diffs_3 = 0

    current_jolt = 0

    while jolt_diffs_1 + jolt_diffs_3 < len(input):
        diff_1 = get_next_jolt_by_diff(input, current_jolt, 1)
        if diff_1:
            # print(f'For {current_jolt} got 1 jolt diff')
            jolt_diffs_1 += 1
            current_jolt = diff_1
            continue

        diff_3 = get_next_jolt_by_diff(input, current_jolt, 3)

        if diff_3:
            # print(f'For {current_jolt} got 3 jolt diff')
            jolt_diffs_3 += 1
            current_jolt = diff_3
            continue

    # built in adapter always 3 jolt diff
    jolt_diffs_3 += 1

    print(f'Part 1: {jolt_diffs_1} x 1 jolt diffs, {jolt_diffs_3} x 3 jolt diffs = {jolt_diffs_1 * jolt_diffs_3}')

    print(f'Final jolt: {current_jolt}')

    possible_combinations = get_possible_combinations(current_jolt + 3)

    print(f'Part 2: {possible_combinations}')