if __name__ == "__main__":
    passwords = []
    with open('./day_02/input.txt') as f:
        passwords = f.readlines()

    valid_passwords = 0

    for password in passwords:
        parts = password.split()
        numbers = parts[0].split("-")
        lowest = int(numbers[0])
        highest = int(numbers[1])
        letter = parts[1].replace(":", "")
        occurences = parts[2].count(letter)
        if occurences >= lowest and occurences <= highest:
            valid_passwords += 1

    print(f"{valid_passwords} valid passwords")

    valid_passwords = 0
    
    for password in passwords:
        parts = password.split()
        numbers = parts[0].split("-")
        lowest = int(numbers[0])
        highest = int(numbers[1])
        letter = parts[1].replace(":", "")
        word = parts[2]
        if word[lowest - 1] == letter and not word[highest - 1] == letter:
            valid_passwords += 1
        elif not word[lowest - 1] == letter and word[highest - 1] == letter:
            valid_passwords += 1

    print(f"{valid_passwords} valid passwords for part two")