def get_instructions():
    instructions = []
    with open('./day_08/input.txt') as f:
        instructions = [line.strip('\n') for line in f]
    return instructions

def is_loop(instructions):
    acc = 0
    cursor = 0
    seen = []
    while True:
        if cursor in seen:
            return True
        seen.append(cursor)
        if cursor > len(instructions) - 1:
            return False
        instruction = instructions[cursor]
        if instruction.startswith("nop"):
            cursor += 1
        elif instruction.startswith("acc"):
            acc += int(instruction.replace("acc ", ""))
            cursor += 1
        elif instruction.startswith("jmp"):
            cursor += int(instruction.replace("jmp ", ""))

def replace_next_jmp(instructions, last_jmp_pos):
    current_pos = last_jmp_pos + 1
    for instruction in instructions[last_jmp_pos + 1:]:
        if instruction.startswith("jmp"):
            instructions[current_pos] = "nop +0"
            return instructions, current_pos
        current_pos += 1


if __name__ == "__main__":
    instructions = get_instructions()

    acc = 0
    cursor = 0

    seen = []

    while True:
        if cursor in seen:
            break
        seen.append(cursor)
        instruction = instructions[cursor]
        if instruction.startswith("nop"):
            cursor += 1
        elif instruction.startswith("acc"):
            acc += int(instruction.replace("acc ", ""))
            cursor += 1
        elif instruction.startswith("jmp"):
            cursor += int(instruction.replace("jmp ", ""))


    print(f'Part 1: {acc}')
    last_jmp_pos = 0

    fixed_instr = None

    while True:
        mod_instr, next_jmp_pos = replace_next_jmp(instructions.copy(), last_jmp_pos)
        if is_loop(mod_instr):
            last_jmp_pos = next_jmp_pos
        else:
            fixed_instr = mod_instr
            break

    acc = 0
    cursor = 0

    seen = []

    while cursor < len(mod_instr):
        if cursor in seen:
            break
        seen.append(cursor)
        instruction = fixed_instr[cursor]
        if instruction.startswith("nop"):
            cursor += 1
        elif instruction.startswith("acc"):
            acc += int(instruction.replace("acc ", ""))
            cursor += 1
        elif instruction.startswith("jmp"):
            cursor += int(instruction.replace("jmp ", ""))
    
    print(f'Part 2: {acc}')