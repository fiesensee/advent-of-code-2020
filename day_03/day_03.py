def get_map():
    rows = []
    with open('./day_03/input.txt') as f:
        rows = [line.strip('\n') for line in f]
    return rows

def count_trees(rows, x_diff, y_diff):
    number_of_trees = 0
    current_row = 0
    current_column = 0

    while current_row < len(rows) - 1:
        current_row = current_row + y_diff
        current_column = current_column + x_diff

        # if current_row > len(rows):
        #     break

        if current_column > len(rows[0]) - 1:
            current_column = current_column - len(rows[0])

        # print(f"{current_row} {current_column}: ", end="")
        next_field = rows[current_row][current_column]
        # print(f"{next_field}")
        
        if next_field == "#":
            number_of_trees += 1

    print(number_of_trees)
    
    return number_of_trees

if __name__ == "__main__":
    rows = get_map()

    print(rows)

    number_of_trees = 0
    current_row = 0
    current_column = 0

    while current_row < len(rows) - 1:
        current_row = current_row + 1
        current_column = current_column + 3

        if current_column > len(rows[0]) - 1:
            current_column = current_column - len(rows[0])

        print(f"{current_row} {current_column}: ", end="")
        next_field = rows[current_row][current_column]
        print(f"{next_field}")
        
        if next_field == "#":
            number_of_trees += 1


    print(f"Part 1: {number_of_trees}")

    # part2_1 = count_trees(rows, 1, 1)
    # part2_2 = count_trees(rows, 3, 1)
    # part2_3 = count_trees(rows, 5, 1)
    # part2_4 = count_trees(rows, 7, 1)
    # part2_5 = count_trees(rows, 1, 2)

    # print(f"1: {part2_1}, 2: {part2_2}, 3: {part2_3}, 4: {part2_4}, 5: {part2_5}")

    part2_result = count_trees(rows, 1, 1) * count_trees(rows, 3, 1) * count_trees(rows, 5, 1) * count_trees(rows, 7, 1) * count_trees(rows, 1, 2)

    print(f"Part 2: {part2_result}")