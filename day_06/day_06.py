def get_answers():
    answers = []
    with open('./day_06/input.txt') as f:
        answers = [line.strip('\n') for line in f]
    return answers

if __name__ == "__main__":
    alphabet = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']

    answers = get_answers()
    print(answers)

    groups = []
    current_group = []
    for answer in answers:
        if answer == '' and current_group != []:
            groups.append(current_group)
            current_group = []
        else:
            current_group.append(answer)
    groups.append(current_group)
    
    print(groups)

    part1 = 0

    for group in groups:
        already_counted = []
        for answer in group:
            for letter in answer:
                if letter not in already_counted:
                    already_counted.append(letter)
        part1 += len(already_counted)

    print(f'Part 1: {part1}')

    part2 = 0

    for group in groups:
        answer_counts = {}
        for answer in group:
            for letter in answer:
                if letter not in answer_counts:
                    answer_counts[letter] = 1
                else:
                    answer_counts[letter] = answer_counts[letter] + 1
        for k, v in answer_counts.items():
            if v == len(group):
                part2 += 1

    print(f'Part 1: {part2}')