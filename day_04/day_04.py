import re

def get_passports():
    passports = []
    data = ""
    with open('./day_04/input.txt') as f:
        data = f.read()
    
    splits = re.split("\n\n", data)

    for split in splits:
        passports.append(split.replace("\n", " "))

    return passports

def get_passports_2():
    passports = []
    data = ""
    with open('./day_04/input.txt') as f:
        data = f.read()
    
    splits = re.split("\n\n", data)
    # print(len(splits))
    # print(splits)

    for split in splits:
        values = split.replace("\n", " ").split(" ")
        passport = {}
        for value in values:
            key_value = value.split(":")
            passport[key_value[0]] = key_value[1]
        # print(passport)
        passports.append(passport)

    return passports


if __name__ == "__main__":
    mandatories = ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"]

    passports = get_passports()

    valid_passports = 0

    for passport in passports:
        is_valid = True
        for mandatory in mandatories:
            if mandatory not in passport:
                is_valid = False
                break
        if is_valid:
            valid_passports += 1

    print(f"Part 1: {valid_passports}")

    passports_2 = get_passports_2()

    valid_passports = 0

    valid_eye_colors = ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"]
   
    for passport in passports_2:
        is_valid = True

        if "byr" not in passport or len(passport["byr"]) != 4 or int(passport["byr"]) < 1920 or int(passport["byr"]) > 2002:
            is_valid = False
            continue

        if "iyr" not in passport or len(passport["iyr"]) != 4 or int(passport["iyr"]) < 2010 or int(passport["iyr"]) > 2020:
            is_valid = False
            continue

        if "eyr" not in passport or len(passport["eyr"]) != 4 or int(passport["eyr"]) < 2020 or int(passport["eyr"]) > 2030:
            is_valid = False
            continue

        if "hgt" not in passport or ("cm" not in passport["hgt"] and "in" not in passport["hgt"]):
            is_valid = False
            continue
        elif "cm" in passport["hgt"]:
                height = int(passport["hgt"].replace("cm", ""))
                if height < 150 or height > 193:
                    is_valid = False
                    continue
        elif "in" in passport["hgt"]:
                height = int(passport["hgt"].replace("in", ""))
                if height < 59 or height > 76:
                    is_valid = False
                    continue

        if "hcl" not in passport:
            is_valid = False
            continue
        else:
            if not re.match("#[0-9a-f]{6}", passport["hcl"]):
                is_valid = False
                continue

        if "ecl" not in passport or len(passport["ecl"]) != 3 or passport["ecl"] not in valid_eye_colors:
            is_valid = False
            continue

        if "pid" not in passport or len(passport["pid"]) != 9:
            is_valid = False
            continue

        if is_valid:
            # print(passport)
            valid_passports += 1

    print(f"Part 2: {valid_passports}")