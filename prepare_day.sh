#! /bin/sh

mkdir -p day_$1
touch day_$1/day_$1.py
touch day_$1/input.txt
touch day_$1/test_input.txt

cat > day_$1/day_$1.py <<- EOM
def get_input():
    input = []
    with open('./day_$1/test_input.txt') as f:
        input = [int(line.strip('\n')) for line in f]
    return input

if __name__ == "__main__":
    input = get_input()
EOM