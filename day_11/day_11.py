import copy

flatten = lambda t: [item for sublist in t for item in sublist]

def get_input():
    input = []
    with open('./day_11/input.txt') as f:
        input = [list(line.strip('\n')) for line in f]
    return input

def print_chairs(chairs):
    for row in chairs:
        for chair in row:
            print(chair, end='')
        print()

def count_occupied_neighboring_chairs(chairs, chair_x, chair_y):
    occupied_chairs = 0

    coordinates = [
        [chair_x - 1, chair_y -1],
        [chair_x, chair_y -1],
        [chair_x + 1, chair_y -1],
        [chair_x - 1, chair_y],
        [chair_x + 1, chair_y],
        [chair_x - 1, chair_y + 1],
        [chair_x, chair_y + 1],
        [chair_x + 1, chair_y + 1],
    ]

    for point in coordinates:
        if point[0] < 0 or point[1] < 0 or len(chairs) <= point[1] or len(chairs[point[1]]) <= point[0]:
            continue
        chair = chairs[point[1]][point[0]]
        if chair == "#":
            occupied_chairs += 1

    return occupied_chairs

def count_occupied_visible_chairs(chairs, chair_x, chair_y):
    occupied_chairs = 0

    coordinates = [
        [chair_x - 1, chair_y -1],
        [chair_x, chair_y -1],
        [chair_x + 1, chair_y -1],
        [chair_x - 1, chair_y],
        [chair_x + 1, chair_y],
        [chair_x - 1, chair_y + 1],
        [chair_x, chair_y + 1],
        [chair_x + 1, chair_y + 1],
    ]

    radius = 1
    invalid_points = 0
    seen_directions = []

    while invalid_points != 8:
        invalid_points = 0
        for point in coordinates:
            if coordinates.index(point) in seen_directions or point[0] < 0 or point[1] < 0 or len(chairs) <= point[1] or len(chairs[point[1]]) <= point[0]:
                seen_directions.append(coordinates.index(point))
                invalid_points += 1
            else:
                chair = chairs[point[1]][point[0]]
                if chair == "#":
                    occupied_chairs += 1
                    seen_directions.append(coordinates.index(point))
                if chair == "L":
                    seen_directions.append(coordinates.index(point))

        radius += 1
        coordinates = [
            [chair_x - radius, chair_y - radius],
            [chair_x, chair_y -radius],
            [chair_x + radius, chair_y - radius],
            [chair_x - radius, chair_y],
            [chair_x + radius, chair_y],
            [chair_x - radius, chair_y + radius],
            [chair_x, chair_y + radius],
            [chair_x + radius, chair_y + radius],
        ]


    return occupied_chairs

def run_computation(chairs):
    next_iter = copy.deepcopy(chairs)

    for y in range(len(chairs)):
        row = chairs[y]
        for x in range(len(row)):
            chair = chairs[y][x]
            onc = count_occupied_neighboring_chairs(chairs, x, y)
            if chair == "L" and onc == 0:
                next_iter[y][x] = "#"
            elif chair == "#" and onc >= 4:
                next_iter[y][x] = "L"

    return next_iter

def run_computation2(chairs):
    next_iter = copy.deepcopy(chairs)

    for y in range(len(chairs)):
        row = chairs[y]
        for x in range(len(row)):
            chair = chairs[y][x]
            onc = count_occupied_visible_chairs(chairs, x, y)
            if chair == "L" and onc == 0:
                next_iter[y][x] = "#"
            elif chair == "#" and onc >= 5:
                next_iter[y][x] = "L"

    return next_iter


if __name__ == "__main__":
    input = get_input()
    # print_chairs(input)
    last_iter = copy.deepcopy(input)
    next_iter = run_computation(input)
    while flatten(last_iter) != flatten(next_iter):
        # print_chairs(last_iter)
        # print("\n==================================\n")
        last_iter = next_iter
        next_iter = run_computation(last_iter)

    part1 = 0
    for row in last_iter:
        for chair in row:
            if chair == "#":
                part1 += 1

    print(f'Part 1: {part1}')

    last_iter = copy.deepcopy(input)
    next_iter = run_computation(input)
    while flatten(last_iter) != flatten(next_iter):
        # print_chairs(next_iter)
        # print("\n==================================\n")
        last_iter = next_iter
        next_iter = run_computation2(last_iter)

    part2 = 0
    for row in last_iter:
        for chair in row:
            if chair == "#":
                part2 += 1

    print(f'Part 2: {part2}')