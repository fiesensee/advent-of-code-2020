import re

def get_raw_rules():
    rules = []
    with open('./day_07/input.txt') as f:
        rules = f.readlines()
    return rules

def get_bag_total(rules, bag_color):
    print(f'getting bag total for {bag_color}')
    # get the bag
    bag = None
    for rule in rules:
        if rule["bag_color"] == bag_color:
            bag = rule
            break
    inner_bags = bag["inner_bags"]
    if inner_bags == []:
        return 1
    sub_total = 0
    for inner_bag in inner_bags:
        sub_total += int(inner_bag["count"]) * get_bag_total(rules, inner_bag["color"])
        print(f'bag color: {rule["bag_color"]}, inner bag color: {inner_bag["color"]}, total: {sub_total}')

    return 1 + sub_total


if __name__ == "__main__":
    raw_rules = get_raw_rules()

    rules = []

    for raw_rule in raw_rules:
        bag_color = re.match(r"(\w+ \w+)", raw_rule).group(1)
        if raw_rule.endswith("contain no other bags."):
            rules.append({
                "bag_color": bag_color,
                "inner_bags": []
            })
        else:
            mg = re.findall(r"(?:(\d+ [\w\s]+)[,.\s]+)", raw_rule)
            inner_bags = []
            for group in mg:
                count = re.match(r"(\d+)", group).group(1)
                color = re.match(r"\d+ ([\w\s]+) bag", group).group(1)
                inner_bags.append({
                    "color": color,
                    "count": count
                })
            rules.append({
                "bag_color": bag_color,
                "inner_bags": inner_bags
            })

    print(rules)

    current_bag_search = ["shiny gold"]
    next_bag_search = []
    total_bags = []
    while True:
        for rule in rules:
            for inner_bag in rule["inner_bags"]:
                if inner_bag["color"] in current_bag_search and rule["bag_color"] not in total_bags:
                    next_bag_search.append(rule["bag_color"])
                    total_bags.append(rule["bag_color"])
        if next_bag_search == []:
            break
        current_bag_search = next_bag_search
        next_bag_search = []


    print(f'Part 1: {len(set(total_bags))}')

    print(f'Part 2: {get_bag_total(rules, "shiny gold") - 1}')

