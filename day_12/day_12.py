import math

def get_input():
    input = []
    with open('./day_12/input.txt') as f:
        input = [line.strip('\n') for line in f]
        input = list(map(lambda x: {"action": x[0], "value": float(x[1:])}, input))
    return input

def change_direction(current_rotation, direction, value):
    new_rotation = None
    new_direction = {}
    if direction == "L":
        new_rotation = current_rotation - value
        if new_rotation < 0:
            new_rotation = 360 + new_rotation
    else:
        new_rotation = (current_rotation + value) % 360

    new_direction["x"] = int(math.sin(math.radians(new_rotation)))
    new_direction["y"] = int(math.cos(math.radians(new_rotation)))

    return new_direction, new_rotation


if __name__ == "__main__":
    input = get_input()

    current_x = 0
    current_y = 0
    current_rotation = 90
    current_direction = {"x": 1, "y": 0}

    for action in input:
        value = action["value"]
        if action["action"] in ["R", "L"]:
            # print(f'Changing direction from {current_rotation} with {action}')
            current_direction, current_rotation = change_direction(current_rotation, action["action"], value)
            # print(f'Got new direction {current_rotation} => {current_direction}')
        else:
            # print(f'Moving with {action}, now at x:{current_x}, y:{current_y}')
            if action["action"] == "F":
                current_x = current_x + (value * current_direction["x"])
                current_y = current_y + (value * current_direction["y"])
            elif action["action"] == "N":
                current_y += value
            elif action["action"] == "E":
                current_x += value
            elif action["action"] == "S":
                current_y -= value
            elif action["action"] == "W":
                current_x -= value
            # print(f'Finished moving, now at x:{current_x}, y:{current_y}')

    print(f'Part 1: {abs(current_x)} + {abs(current_y)} = {abs(current_x) + abs(current_y)}')


    current_x = 0
    current_y = 0
    waypoint_x = 10
    waypoint_y = 1
    current_rotation = 90
    current_direction = {"x": 1, "y": 0}

    for action in input:
        value = action["value"]
        if action["action"] in ["R", "L"]:
            # print(f'Changing off waypoint from x:{waypoint_x}, y:{waypoint_y} with {action}')
            # current_direction, current_rotation = change_direction(current_rotation, action["action"], value)
            if action["action"] == "R":
                value = 360 - value
                # print(f"Right turning: {value}")
            new_waypoint_x = waypoint_x * int(math.cos(math.radians(value))) - waypoint_y * int(math.sin(math.radians(value)))
            new_waypoint_y = waypoint_y * int(math.cos(math.radians(value))) + waypoint_x * int(math.sin(math.radians(value)))
            # print(f'Got new waypoint x:{new_waypoint_x}, y:{new_waypoint_y}')
            waypoint_x = new_waypoint_x
            waypoint_y = new_waypoint_y
        else:
            # print(f'Moving with {action}, now at x:{current_x}, y:{current_y}')
            if action["action"] == "F":
                current_x = current_x + (waypoint_x * value)
                current_y = current_y + (waypoint_y * value)
            elif action["action"] == "N":
                waypoint_y += value
            elif action["action"] == "E":
                waypoint_x += value
            elif action["action"] == "S":
                waypoint_y -= value
            elif action["action"] == "W":
                waypoint_x -= value
            # print(f'Finished moving, now at x:{current_x}, y:{current_y}; Waypoint at x:{waypoint_x}, y:{waypoint_y}')

    print(f'Part 2: {abs(current_x)} + {abs(current_y)} = {abs(current_x) + abs(current_y)}')